import 'dotenv/config';
import express from 'express';

const app = express();

let tasks = [{ "id": 1, "title": "Call staff meeting", "done": false },
{ "id": 2, "title": "Lunch with client", "done": false },
{ "id": 3, "title": "Softball after work", "done": false }];

app.get('/tasks', (req, res) => {
  return res.send(tasks);
});

app.get('/tasks/:taskId', (req, res) => {
  let newTasks = tasks.filter((item)=>item.id == req.params.taskId);
  let result = (newTasks && newTasks.length == 1)?newTasks[0]:[];
  return res.send(result);
});

app.get('/', (req, res) => {
  return res.send('Received a GET HTTP method.');
});

app.post('/', (req, res) => {
  return res.send('Received a POST HTTP method');
});

app.put('/', (req, res) => {
  return res.send('Received a PUT HTTP method');
});

app.delete('/', (req, res) => {
  return res.send('Received a DELETE HTTP method');
});

app.listen(process.env.PORT, () =>
  console.log(`Example app listening on port ${process.env.PORT}!`),
);